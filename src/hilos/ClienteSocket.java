/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hilos;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author macpro1
 */
public class ClienteSocket {
    public static void main(String args[]){
        boolean bSalir = false;
        String respuesta = "";
        String mensaje = "";
        
        BufferedReader reader =
                   new BufferedReader(new InputStreamReader(System.in));
        
        try {
            Socket conexion = new Socket("10.25.0.21",1001);
            
            OutputStream     os  =  conexion.getOutputStream();
            DataOutputStream dos = new DataOutputStream(os);                       
            
            InputStream     is  = conexion.getInputStream();
            DataInputStream dis = new DataInputStream(is);
            
            // Al conectar, lo primero es esperar una respuesta
            respuesta = dis.readUTF();
            System.out.println(respuesta);

            // Suponemos que lo primero, es que nos pide nuestro nombre
            mensaje = reader.readLine();            
            dos.writeUTF(mensaje);
            
            while(!bSalir){
                respuesta = dis.readUTF();
                System.out.println(respuesta);

                // Suponemos que lo primero, es que nos pide nuestro nombre
                mensaje = reader.readLine();            
                dos.writeUTF(mensaje);

                if (mensaje.equals("salir")){
                    bSalir = true;
                }
            }
            
            conexion.close();
        } catch (IOException ex) {
            Logger.getLogger(ClienteSocket.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
