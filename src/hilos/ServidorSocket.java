/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hilos;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket; // Server el que esta atento a conexiones
import java.net.Socket;  // Cliente o la conexion cliente del "server"
import java.util.logging.Level;
import java.util.logging.Logger;
import java.time.LocalDate;
/**
 *
 * @author macpro1
 */
public class ServidorSocket {
    
    public static void main(String args[]){
        
        try {
            ServerSocket servidor = new ServerSocket(1001);
            System.out.println("Iniciando proceso servidor");
            while(true){   
                new ProcesoHilo(servidor.accept()).start();
            }
        } catch (IOException ex) {
            Logger.getLogger(ServidorSocket.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}

class ProcesoHilo extends Thread {
    Socket conexion;

    ProcesoHilo(Socket cnx){
        conexion = cnx;
    }

    public void run(){
        
        String msgEnviar = "";
        String msgRespuesta = "";
        boolean bSalir = false;
        
        try {      
            System.out.printf("Recibiendo conexion desde %s.\n",conexion.getInetAddress().toString());   
                        
            // Canal para la entrada
            InputStream is = conexion.getInputStream();
            DataInputStream dis = new DataInputStream(is);
            // Canal para la salida
            OutputStream os = conexion.getOutputStream();            
            DataOutputStream dos = new DataOutputStream(os);
            
            dos.writeUTF("Bienvenido a Servidor Chat. Escriba su nombre ");
            
            msgRespuesta = dis.readUTF();
            
            System.out.printf("Se conecto: %s.\n",msgRespuesta); 
            
            while (!bSalir){                
                 msgEnviar = "Respuesta eco: "+msgRespuesta;                 
                 dos.writeUTF(msgEnviar);
                                  
                 msgRespuesta = dis.readUTF();                
                 System.out.printf("Recibiendo desde %s - %s",conexion.getInetAddress().toString(),msgRespuesta);
                 
                 if(msgRespuesta.equals("salir")){
                     bSalir = true;
                 }
            }                                             
            conexion.close();
        } catch (IOException io){
            
        }
    }   
}