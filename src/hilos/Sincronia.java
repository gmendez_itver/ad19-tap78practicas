/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hilos;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author macpro1
 */
public class Sincronia {
    
    int x = 0;
    
    public static void main(String args[]){
        new Sincronia().start();
    }
    
    public void start(){
        ProcesoHilo ph1 = new ProcesoHilo(this,"ph1",500);
        ProcesoHilo ph2 = new ProcesoHilo(this,"ph2",1500);
        
        ph1.start();
        ph2.start();
        
    }
    
}

class ProcesoHilo extends Thread {
    String nombre = "";
    Sincronia padre = null;
    int tiempo = 0;
            
    ProcesoHilo(Sincronia padre, String nombre, int tiempo){
        this.padre = padre;
        this.nombre = nombre;
        this.tiempo = tiempo;
    }
    
    @Override
    public void run(){
        int y = 0;
        int r = this.random();
        this.padre.x = r;
        
        System.out.printf("%s - Numero random r = %d\n",this.nombre,r);
        this.ciclo(r);

    }
    
    synchronized void ciclo(int r){
        for (int i=0; i<100; i++){
            this.padre.x = i;
            System.out.printf("%s - Contador i = %d, x = %d\n",this.nombre,i,this.padre.x); 
            try {
                Thread.sleep(this.tiempo);
            } catch (InterruptedException ex) {
                Logger.getLogger(ProcesoHilo.class.getName()).log(Level.SEVERE, null, ex);
            }
        } 
    }
    
    int random(){
        int max = 100;
        int min = 1;
        Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
    }
    
}