package practicas;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class Practica1 {
    JFrame ventana;

    public static void main(String[] args) {
        Practica1 app = new Practica1();
        
        app.run();
    }
    
    public void run(){
        ventana = new JFrame("Practica 1");
        ventana.setSize(300,200);
        ventana.setLayout(new FlowLayout());
        ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JLabel lblNombre = new JLabel("Escriba su nombre");
        JTextField tfNombre = new JTextField(10);
        JButton btnAccion = new JButton("Saludar");
        
        btnAccion.addActionListener(
            new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent e) {
                    JOptionPane.showMessageDialog(null,"Hola! "+tfNombre.getText());

                }

            }
        );
        
        ventana.add(lblNombre);
        ventana.add(tfNombre);
        ventana.add(btnAccion);
        
        ventana.setVisible(true);
    }
}
